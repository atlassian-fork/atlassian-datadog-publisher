Home of the Atlassian Datadog Publisher, which supports sending metrics to Datadog. It's meant to be a thin helper abstraction over low-level Datadog API.
